package main

import (
    "database/sql"
    "fmt"
	_"github.com/lib/pq"
	"log"
	"bufio"
	"os"
	"strings"
)

func createDatabase() {

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {	
		log.Fatal(err)
	}

	row := db.QueryRow(`SELECT EXISTS(SELECT datname FROM pg_catalog.pg_database WHERE datname = 'informaciontarjeta');`)
	var exists bool
	err = row.Scan(&exists)
	if err != nil {	
		log.Fatal(err)
	}

	if exists == false {
	    _, err = db.Exec(`CREATE DATABASE informaciontarjeta;`)
	    if err != nil {	
			log.Fatal(err)
		}
		fmt.Println("La base de datos 'informaciontarjeta' ha sido creada")
	}else{
		fmt.Println("La base de datos 'informaciontarjeta' ya existe")
	}	
}

func deleteDatabase() {

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {	
		log.Fatal(err)
	}
	defer db.Close()

	row := db.QueryRow(`SELECT EXISTS(SELECT datname FROM pg_catalog.pg_database WHERE datname = 'informaciontarjeta');`)
	var exists bool
	err = row.Scan(&exists)
	if err != nil {	
		log.Fatal(err)
	}

	if exists == false {
		fmt.Println("La base de datos 'informaciontarjeta' no existe")
	}else{
		_, err = db.Exec(`DROP DATABASE informaciontarjeta;`)
		if err != nil {
    	    log.Fatal(err)
    	}
		fmt.Println("La base de datos 'informaciontarjeta' ha sido borrada")
	}		
}

func createTables(){

	//Abro la base de datos 'informaciontarjeta'
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()
	
	//Creo todas las tablas si no existe ya dentro de 'informaciontarjeta'
	_, err = db.Exec(`create table if not exists cliente(nrocliente int, nombre text, apellido text, domicilio text, telefono char(12))`)
    if err != nil {
		log.Fatal(err)
    }

	_, err = db.Exec(`create table if not exists tarjeta(nrotarjeta char(16), nrocliente int, validadesde char(6), validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10))`)
    if err != nil {
		log.Fatal(err)
    }
	
	_, err = db.Exec(`create table if not exists comercio(nrocomercio int, nombre text, domicilio text, codigopostal char(8), telefono char(12))`)
	if err != nil {
		log.Fatal(err)
	}
	
	_, err = db.Exec(`create table if not exists compra(nrooperacion int, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean)`)
	if err != nil {
		log.Fatal(err)
	}
	
	_, err = db.Exec(`create table if not exists rechazo(nrorechazo int, nrotarjeta char(16),nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text)`)
	if err != nil {
		log.Fatal(err)
	}
	
	_, err = db.Exec(`create table if not exists cierre(año int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`create table if not exists cabecera(nroresumen int, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date, vence date, total decimal(8,2))`)
	if err != nil {
		log.Fatal(err)
	}
	
	_, err = db.Exec(`create table if not exists detalle(nroresumen int, nrolinea int, fecha date,nombrecomercio text, monto decimal(7,2))`)
	if err != nil {
		log.Fatal(err)
	}
	
	_, err = db.Exec(`create table if not exists alerta(nroalerta int, nrotarjeta char(16), fecha timestamp, nrorechazo int, codalerta int, descripcion text)`)
	if err != nil {
		log.Fatal(err)
	}
	
	_, err = db.Exec(`create table if not exists consumo(nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2))`)
	if err != nil {
		log.Fatal(err)
	}
}

func createTestClientes(){

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Inserto los clientes de prueba si no están ya agregados a 'informaciontarjeta'
	rows, err := db.Query("SELECT nrocliente FROM cliente;")
	if err != nil {
    	log.Fatal(err)
	}
	defer rows.Close()	

	cont :=0
	for rows.Next() {    	
		cont = cont + 1
	}
	if err := rows.Err(); err != nil {
    	log.Fatal(err)
	}
	if cont<20 {
		_, err = db.Exec(`insert into cliente values(0::int, 'Agustin', 'Cirillo', 'Bella Vista', 12340000);
		insert into cliente values(1::int, 'Brain', 'De JCPa', 'JoseC Paz', 12340001);
		insert into cliente values(2::int, 'Camila', 'Rivero', 'Bella Vista', 12340002);
		insert into cliente values(3::int, 'Dario', 'Dominguez', 'Bella Vista', 12340003);
		insert into cliente values(4::int, 'Alberto', 'Fernandez', 'Ezeiza', 12340004);
		insert into cliente values(5::int, 'Cristina', 'Fernandez', 'Recoleta', 12340005);
		insert into cliente values(6::int, 'Mauricio', 'Macri', 'Malvinas Argentinas', 12340006);
		insert into cliente values(7::int, 'Hernan', 'Czeninskiknfgujg', 'Malvinas Argentinas', 12340007);
		insert into cliente values(8::int, 'Yvael', 'Tercero', 'La Boca', 12340008);
		insert into cliente values(9::int, 'Javier', 'Lopez', 'Ituzaingo', 12340009);
		insert into cliente values(10::int, 'Kevin', 'Rodriguez', 'Buenos Aires', 12340010);
		insert into cliente values(11::int, 'Hernan', 'Lucifer', 'Boca', 12340011);
		insert into cliente values(12::int, 'Matias', 'Alvarez', 'Pablo Nogues', 12340012);
		insert into cliente values(13::int, 'Misato', 'Katsuragi', 'Tokio', 12340013);
		insert into cliente values(14::int, 'Jebus', 'Cristo', 'Vaticano', 12340014);
		insert into cliente values(15::int, 'Goku', 'Kakaroto', 'Montania Paoz', 12340015);
		insert into cliente values(16::int, 'Gohan', 'Son Gohan', 'Montania Paoz', 12340016);
		insert into cliente values(17::int, 'Ritsuko', 'Akagi', 'Bella Vista', 12340017);
		insert into cliente values(18::int, 'Santiago', 'Huck', 'Villa de Mayo', 12340018);
		insert into cliente values(19::int, 'Sebastian', 'Soldano', 'Villa Soldati', 12340019);`)
    	if err != nil {
    	    log.Fatal(err)
		}		
	}else{
		fmt.Println("Los clientes ya están guardados en la base de datos 'informaciontarjeta'")
	}
}

func createTestComercios(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Inserto los comercios de prueba si no están ya agregados a 'informaciontarjeta'
	rows2, err := db.Query("SELECT nrocomercio FROM comercio;")
	if err != nil {
    	log.Fatal(err)
	}
	defer rows2.Close()

	cont2 :=0
	for rows2.Next() {  
		cont2 = cont2 + 1
	}
	if err := rows2.Err(); err != nil {
    	log.Fatal(err)
	}

	if cont2<20 {
		_, err = db.Exec(`insert into comercio values(0::int, 'A', 'Malvinas Argentinas', 1614::char(8), 22340000);
		insert into comercio values(1::int, 'B', 'Malvinas Argentinas', 1614::char(8), 22340001);
		insert into comercio values(2::int, 'C', 'Bella Vista', 1661::char(8), 22340002);
		insert into comercio values(3::int, 'D', 'Pablo Nogues', 1616::char(8), 22340003);
		insert into comercio values(4::int, 'E', 'La Boca', 1265::char(8), 22340004);
		insert into comercio values(5::int, 'F', 'Bella Vista', 1661::char(8), 22340005);
		insert into comercio values(6::int, 'G', 'Bella Vista', 1661::char(8), 22340006);
		insert into comercio values(7::int, 'H', 'Pablo Nogues', 1616::char(8), 22340007);
		insert into comercio values(8::int, 'I', 'Malvinas Argentinas', 1614::char(8), 22340008);
		insert into comercio values(9::int, 'J', 'La Boca', 1265::char(8), 22340009);
		insert into comercio values(10::int, 'K', 'La Boca', 1265::char(8), 22340010);
		insert into comercio values(11::int, 'L', 'Bella Vista', 1661::char(8), 22340011);
		insert into comercio values(12::int, 'M', 'Pablo Nogues', 1616::char(8), 22340012);
		insert into comercio values(13::int, 'N', 'Malvinas Argentinas', 1614::char(8), 22340013);
		insert into comercio values(14::int, 'O', 'Malvinas Argentinas', 1614::char(8), 22340014);
		insert into comercio values(15::int, 'P', 'Bella Vista', 1661::char(8), 22340015);
		insert into comercio values(16::int, 'Q', 'La Boca', 1265::char(8), 22340016);
		insert into comercio values(17::int, 'R', 'Malvinas Argentinas', 1614::char(8), 22340017);
		insert into comercio values(18::int, 'S', 'Malvinas Argentinas', 1614::char(8), 22340018);
		insert into comercio values(19::int, 'T', 'Bella Vista', 1661::char(8), 22340019);`)
    	if err != nil {
    	    log.Fatal(err)
    	}
	}else{
		fmt.Println("Los comercios ya están guardados en la base de datos 'informaciontarjeta'")
	}
}

func createTestTarjetas(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Inserto las tarjeta de prueba si no están ya agregados a 'informaciontarjeta'
	rows3, err := db.Query("SELECT nrotarjeta FROM tarjeta;")
	if err != nil {
    	log.Fatal(err)
	}
	defer rows3.Close()

	cont3 :=0
	for rows3.Next() {  
		cont3 = cont3 + 1
	}
	if err := rows3.Err(); err != nil {
    	log.Fatal(err)
	}

	if cont3<22{
		_, err = db.Exec(`insert into tarjeta values(4000123412340001::char(16), 0::int, 202006::char(6), 202006::char(6), 0001::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340002::char(16), 1::int, 201106::char(6), 202006::char(6), 0002::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340003::char(16), 2::int, 201106::char(6), 202006::char(6), 1333::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340004::char(16), 3::int, 201106::char(6), 202006::char(6), 0004::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340005::char(16), 4::int, 201106::char(6), 202006::char(6), 0005::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340006::char(16), 5::int, 201106::char(6), 202006::char(6), 0006::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340007::char(16), 6::int, 201106::char(6), 202006::char(6), 0007::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340008::char(16), 7::int, 201106::char(6), 202006::char(6), 0008::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340009::char(16), 8::int, 201106::char(6), 202006::char(6), 0009::char(4), 50000::decimal(8,2), 'vigente'::char(10));

		--Inserto una tarjeta expirada en su fecha de vencimiento
		insert into tarjeta values(4000123412340011::char(16), 9::int, 201106::char(6), 201506::char(6), 0011::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		
		insert into tarjeta values(4000123412340012::char(16), 10::int, 201106::char(6), 202006::char(6), 0012::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340013::char(16), 11::int, 201106::char(6), 202006::char(6), 0013::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340014::char(16), 12::int, 201106::char(6), 202006::char(6), 0014::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340015::char(16), 13::int, 201106::char(6), 202006::char(6), 0015::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340016::char(16), 8::int, 201106::char(6), 202006::char(6), 0016::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340017::char(16), 14::int, 201106::char(6), 202006::char(6), 0017::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340018::char(16), 15::int, 201106::char(6), 202006::char(6), 0018::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340019::char(16), 3::int, 201106::char(6), 202006::char(6), 0019::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340021::char(16), 16::int, 201106::char(6), 202006::char(6), 0021::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340022::char(16), 17::int, 201106::char(6), 202006::char(6), 0022::char(4), 50000::decimal(8,2), 'vigente'::char(10));
		insert into tarjeta values(4000123412340023::char(16), 18::int, 201106::char(6), 202006::char(6), 0023::char(4), 50000::decimal(8,2), 'vigente'::char(10));

		--Inserto una tarjeta con estado 'suspendida'
		insert into tarjeta values(4000123412340024::char(16), 19::int, 201106::char(6), 202006::char(6), 0024::char(4), 50000::decimal(8,2), 'suspendida'::char(10));`)
    	if err != nil {
    	    log.Fatal(err)
    	}
	}else{
		fmt.Println("Las tarjetas ya están guardadas en la base de datos 'informaciontarjeta'")
	}

}

func createTestCierres(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Inserto los cierres del año 2019 si no están ya agregados a 'informaciontarjeta'
	rows4, err := db.Query("SELECT año FROM cierre;")
	if err != nil {
    	log.Fatal(err)
	}
	defer rows4.Close()

	cont4 :=0
	for rows4.Next() {  
		cont4 = cont4 + 1
	}
	if err := rows4.Err(); err != nil {
    	log.Fatal(err)
	}

	if cont4<119{
		_, err = db.Exec(`insert into cierre values(2019::int, 1::int, 0::int, '2019-01-01', '2019-02-01', '2019-02-11');
		insert into cierre values(2019::int, 1::int, 1::int, '2019-01-02', '2019-02-02', '2019-02-12');
		insert into cierre values(2019::int, 1::int, 2::int, '2019-01-03', '2019-02-03', '2019-02-13');
		insert into cierre values(2019::int, 1::int, 3::int, '2019-01-04', '2019-02-04', '2019-02-14');
		insert into cierre values(2019::int, 1::int, 4::int, '2019-01-05', '2019-02-05', '2019-02-15');
		insert into cierre values(2019::int, 1::int, 5::int, '2019-01-06', '2019-02-06', '2019-02-16');
		insert into cierre values(2019::int, 1::int, 6::int, '2019-01-07', '2019-02-07', '2019-02-17');
		insert into cierre values(2019::int, 1::int, 7::int, '2019-01-08', '2019-02-08', '2019-02-18');
		insert into cierre values(2019::int, 1::int, 8::int, '2019-01-09', '2019-02-09', '2019-02-19');
		insert into cierre values(2019::int, 1::int, 9::int, '2019-01-10', '2019-02-10', '2019-02-20');
		insert into cierre values(2019::int, 2::int, 0::int, '2019-02-01', '2019-03-01', '2019-03-11');
		insert into cierre values(2019::int, 2::int, 1::int, '2019-02-02', '2019-03-02', '2019-03-12');
		insert into cierre values(2019::int, 2::int, 2::int, '2019-02-03', '2019-03-03', '2019-03-13');
		insert into cierre values(2019::int, 2::int, 3::int, '2019-02-04', '2019-03-04', '2019-03-14');
		insert into cierre values(2019::int, 2::int, 4::int, '2019-02-05', '2019-03-05', '2019-03-15');
		insert into cierre values(2019::int, 2::int, 5::int, '2019-02-06', '2019-03-06', '2019-03-16');
		insert into cierre values(2019::int, 2::int, 6::int, '2019-02-07', '2019-03-07', '2019-03-17');
		insert into cierre values(2019::int, 2::int, 7::int, '2019-02-08', '2019-03-08', '2019-03-18');
		insert into cierre values(2019::int, 2::int, 8::int, '2019-02-09', '2019-03-09', '2019-03-19');
		insert into cierre values(2019::int, 2::int, 9::int, '2019-02-10', '2019-03-10', '2019-03-20');
		insert into cierre values(2019::int, 3::int, 0::int, '2019-03-01', '2019-04-01', '2019-04-11');
		insert into cierre values(2019::int, 3::int, 1::int, '2019-03-02', '2019-04-02', '2019-04-12');
		insert into cierre values(2019::int, 3::int, 2::int, '2019-03-03', '2019-04-03', '2019-04-13');
		insert into cierre values(2019::int, 3::int, 3::int, '2019-03-04', '2019-04-04', '2019-04-14');
		insert into cierre values(2019::int, 3::int, 4::int, '2019-03-05', '2019-04-05', '2019-04-15');
		insert into cierre values(2019::int, 3::int, 5::int, '2019-03-06', '2019-04-06', '2019-04-16');
		insert into cierre values(2019::int, 3::int, 6::int, '2019-03-07', '2019-04-07', '2019-04-17');
		insert into cierre values(2019::int, 3::int, 7::int, '2019-03-08', '2019-04-08', '2019-04-18');
		insert into cierre values(2019::int, 3::int, 8::int, '2019-03-09', '2019-04-09', '2019-04-19');
		insert into cierre values(2019::int, 3::int, 9::int, '2019-03-10', '2019-04-10', '2019-04-20');
		insert into cierre values(2019::int, 4::int, 0::int, '2019-04-01', '2019-05-01', '2019-05-11');
		insert into cierre values(2019::int, 4::int, 1::int, '2019-04-02', '2019-05-02', '2019-05-12');
		insert into cierre values(2019::int, 4::int, 2::int, '2019-04-03', '2019-05-03', '2019-05-13');
		insert into cierre values(2019::int, 4::int, 3::int, '2019-04-04', '2019-05-04', '2019-05-14');
		insert into cierre values(2019::int, 4::int, 4::int, '2019-04-05', '2019-05-05', '2019-05-15');
		insert into cierre values(2019::int, 4::int, 5::int, '2019-04-06', '2019-05-06', '2019-05-16');
		insert into cierre values(2019::int, 4::int, 6::int, '2019-04-07', '2019-05-07', '2019-05-17');
		insert into cierre values(2019::int, 4::int, 7::int, '2019-04-08', '2019-05-08', '2019-05-18');
		insert into cierre values(2019::int, 4::int, 8::int, '2019-04-09', '2019-05-09', '2019-05-19');
		insert into cierre values(2019::int, 4::int, 9::int, '2019-04-10', '2019-05-10', '2019-05-20');
		insert into cierre values(2019::int, 5::int, 0::int, '2019-05-01', '2019-06-01', '2019-06-11');
		insert into cierre values(2019::int, 5::int, 1::int, '2019-05-02', '2019-06-02', '2019-06-12');
		insert into cierre values(2019::int, 5::int, 2::int, '2019-05-03', '2019-06-03', '2019-06-13');
		insert into cierre values(2019::int, 5::int, 3::int, '2019-05-04', '2019-06-04', '2019-06-14');
		insert into cierre values(2019::int, 5::int, 4::int, '2019-05-05', '2019-06-05', '2019-06-15');
		insert into cierre values(2019::int, 5::int, 5::int, '2019-05-06', '2019-06-06', '2019-06-16');
		insert into cierre values(2019::int, 5::int, 6::int, '2019-05-07', '2019-06-07', '2019-06-17');
		insert into cierre values(2019::int, 5::int, 7::int, '2019-05-08', '2019-06-08', '2019-06-18');
		insert into cierre values(2019::int, 5::int, 8::int, '2019-05-09', '2019-06-09', '2019-06-19');
		insert into cierre values(2019::int, 5::int, 9::int, '2019-05-10', '2019-06-10', '2019-06-20');
		insert into cierre values(2019::int, 6::int, 0::int, '2019-06-01', '2019-07-01', '2019-07-11');
		insert into cierre values(2019::int, 6::int, 1::int, '2019-06-02', '2019-07-02', '2019-07-12');
		insert into cierre values(2019::int, 6::int, 2::int, '2019-06-03', '2019-07-03', '2019-07-13');
		insert into cierre values(2019::int, 6::int, 3::int, '2019-06-04', '2019-07-04', '2019-07-14');
		insert into cierre values(2019::int, 6::int, 4::int, '2019-06-05', '2019-07-05', '2019-07-15');
		insert into cierre values(2019::int, 6::int, 5::int, '2019-06-06', '2019-07-06', '2019-07-16');
		insert into cierre values(2019::int, 6::int, 6::int, '2019-06-07', '2019-07-07', '2019-07-17');
		insert into cierre values(2019::int, 6::int, 7::int, '2019-06-08', '2019-07-08', '2019-07-18');
		insert into cierre values(2019::int, 6::int, 8::int, '2019-06-09', '2019-07-09', '2019-07-19');
		insert into cierre values(2019::int, 6::int, 9::int, '2019-06-10', '2019-07-10', '2019-07-20');
		insert into cierre values(2019::int, 7::int, 0::int, '2019-07-01', '2019-08-01', '2019-08-11');
		insert into cierre values(2019::int, 7::int, 1::int, '2019-07-02', '2019-08-02', '2019-08-12');
		insert into cierre values(2019::int, 7::int, 2::int, '2019-07-03', '2019-08-03', '2019-08-13');
		insert into cierre values(2019::int, 7::int, 3::int, '2019-07-04', '2019-08-04', '2019-08-14');
		insert into cierre values(2019::int, 7::int, 4::int, '2019-07-05', '2019-08-05', '2019-08-15');
		insert into cierre values(2019::int, 7::int, 5::int, '2019-07-06', '2019-08-06', '2019-08-16');
		insert into cierre values(2019::int, 7::int, 6::int, '2019-07-07', '2019-08-07', '2019-08-17');
		insert into cierre values(2019::int, 7::int, 7::int, '2019-07-08', '2019-08-08', '2019-08-18');
		insert into cierre values(2019::int, 7::int, 8::int, '2019-07-09', '2019-08-09', '2019-08-19');
		insert into cierre values(2019::int, 7::int, 9::int, '2019-07-10', '2019-08-10', '2019-08-20');
		insert into cierre values(2019::int, 8::int, 0::int, '2019-08-01', '2019-09-01', '2019-09-11');
		insert into cierre values(2019::int, 8::int, 1::int, '2019-08-02', '2019-09-02', '2019-09-12');
		insert into cierre values(2019::int, 8::int, 2::int, '2019-08-03', '2019-09-03', '2019-09-13');
		insert into cierre values(2019::int, 8::int, 3::int, '2019-08-04', '2019-09-04', '2019-09-14');
		insert into cierre values(2019::int, 8::int, 4::int, '2019-08-05', '2019-09-05', '2019-09-15');
		insert into cierre values(2019::int, 8::int, 5::int, '2019-08-06', '2019-09-06', '2019-09-16');
		insert into cierre values(2019::int, 8::int, 6::int, '2019-08-07', '2019-09-07', '2019-09-17');
		insert into cierre values(2019::int, 8::int, 7::int, '2019-08-08', '2019-09-08', '2019-09-18');
		insert into cierre values(2019::int, 8::int, 8::int, '2019-08-09', '2019-09-09', '2019-09-19');
		insert into cierre values(2019::int, 8::int, 9::int, '2019-08-10', '2019-09-10', '2019-09-20');
		insert into cierre values(2019::int, 9::int, 0::int, '2019-09-01', '2019-10-01', '2019-10-11');
		insert into cierre values(2019::int, 9::int, 1::int, '2019-09-02', '2019-10-02', '2019-10-12');
		insert into cierre values(2019::int, 9::int, 2::int, '2019-09-03', '2019-10-03', '2019-10-13');
		insert into cierre values(2019::int, 9::int, 3::int, '2019-09-04', '2019-10-04', '2019-10-14');
		insert into cierre values(2019::int, 9::int, 4::int, '2019-09-05', '2019-10-05', '2019-10-15');
		insert into cierre values(2019::int, 9::int, 5::int, '2019-09-06', '2019-10-06', '2019-10-16');
		insert into cierre values(2019::int, 9::int, 6::int, '2019-09-07', '2019-10-07', '2019-10-17');
		insert into cierre values(2019::int, 9::int, 7::int, '2019-09-08', '2019-10-08', '2019-10-18');
		insert into cierre values(2019::int, 9::int, 8::int, '2019-09-09', '2019-10-09', '2019-10-19');
		insert into cierre values(2019::int, 9::int, 9::int, '2019-09-10', '2019-10-10', '2019-10-20');
		insert into cierre values(2019::int, 10::int, 0::int, '2019-10-01', '2019-11-01', '2019-11-11');
		insert into cierre values(2019::int, 10::int, 1::int, '2019-10-02', '2019-11-02', '2019-11-12');
		insert into cierre values(2019::int, 10::int, 2::int, '2019-10-03', '2019-11-03', '2019-11-13');
		insert into cierre values(2019::int, 10::int, 3::int, '2019-10-04', '2019-11-04', '2019-11-14');
		insert into cierre values(2019::int, 10::int, 4::int, '2019-10-05', '2019-11-05', '2019-11-15');
		insert into cierre values(2019::int, 10::int, 5::int, '2019-10-06', '2019-11-06', '2019-11-16');
		insert into cierre values(2019::int, 10::int, 6::int, '2019-10-07', '2019-11-07', '2019-11-17');
		insert into cierre values(2019::int, 10::int, 7::int, '2019-10-08', '2019-11-08', '2019-11-18');
		insert into cierre values(2019::int, 10::int, 8::int, '2019-10-09', '2019-11-09', '2019-11-19');
		insert into cierre values(2019::int, 10::int, 9::int, '2019-10-10', '2019-11-10', '2019-11-20');
		insert into cierre values(2019::int, 11::int, 0::int, '2019-11-01', '2019-12-01', '2019-12-11');
		insert into cierre values(2019::int, 11::int, 1::int, '2019-11-02', '2019-12-02', '2019-12-12');
		insert into cierre values(2019::int, 11::int, 2::int, '2019-11-03', '2019-12-03', '2019-12-13');
		insert into cierre values(2019::int, 11::int, 3::int, '2019-11-04', '2019-12-04', '2019-12-14');
		insert into cierre values(2019::int, 11::int, 4::int, '2019-11-05', '2019-12-05', '2019-12-15');
		insert into cierre values(2019::int, 11::int, 5::int, '2019-11-06', '2019-12-06', '2019-12-16');
		insert into cierre values(2019::int, 11::int, 6::int, '2019-11-07', '2019-12-07', '2019-12-17');
		insert into cierre values(2019::int, 11::int, 7::int, '2019-11-08', '2019-12-08', '2019-12-18');
		insert into cierre values(2019::int, 11::int, 8::int, '2019-11-09', '2019-12-09', '2019-12-19');
		insert into cierre values(2019::int, 11::int, 9::int, '2019-11-10', '2019-12-10', '2019-12-20');
		insert into cierre values(2019::int, 12::int, 0::int, '2019-12-01', '2019-01-01', '2019-01-11');
		insert into cierre values(2019::int, 12::int, 1::int, '2019-12-02', '2019-01-02', '2019-01-12');
		insert into cierre values(2019::int, 12::int, 2::int, '2019-12-03', '2019-01-03', '2019-01-13');
		insert into cierre values(2019::int, 12::int, 3::int, '2019-12-04', '2019-01-04', '2019-01-14');
		insert into cierre values(2019::int, 12::int, 4::int, '2019-12-05', '2019-01-05', '2019-01-15');
		insert into cierre values(2019::int, 12::int, 5::int, '2019-12-06', '2019-01-06', '2019-01-16');
		insert into cierre values(2019::int, 12::int, 6::int, '2019-12-07', '2019-01-07', '2019-01-17');
		insert into cierre values(2019::int, 12::int, 7::int, '2019-12-08', '2019-01-08', '2019-01-18');
		insert into cierre values(2019::int, 12::int, 8::int, '2019-12-09', '2019-01-09', '2019-01-19');
		insert into cierre values(2019::int, 12::int, 9::int, '2019-12-10', '2019-01-10', '2019-01-20');
		`)
    	if err != nil {
    	    log.Fatal(err)
    	}
	}else{
		fmt.Println("Los cierres ya están guardados en la base de datos 'informaciontarjeta'")
	}
}

func createTestConsumos(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Inserto los consumos de prueba si no están ya agregados a 'informaciontarjeta'
	rows5, err := db.Query("SELECT nrotarjeta FROM consumo;")
	if err != nil {
    	log.Fatal(err)
	}
	defer rows5.Close()

	cont5 :=0
	for rows5.Next() {  
		cont5 = cont5 + 1
	}
	if err := rows5.Err(); err != nil {
    	log.Fatal(err)
	}

	if cont5<13{
		_, err = db.Exec(`insert into consumo values(4000123412340006::char(16), 0006::char(4), 1::int, 5000::decimal(7,2));
		insert into consumo values(4000123412340006::char(16), 0006::char(4), 1::int, 3500::decimal(7,2));
		
		--Compra con tarjeta vencida
		insert into consumo values(4000123412340011::char(16), 0011::char(4), 5::int, 5000::decimal(7,2));

		--Compra con tarjeta inválida
		insert into consumo values(4000123412341003::char(16), 0003::char(4), 3::int, 1500::decimal(7,2));
		
		--Compra con tarjeta suspendida
		insert into consumo values(4000123412340024::char(16), 0024::char(4), 3::int, 1500::decimal(7,2));
		
		--Compra con codigo de seguridad inválido
		insert into consumo values(4000123412340003::char(16), 0003::char(4), 3::int, 1500::decimal(7,2));

		--Compra excediendo el limite de la tarjeta 2 veces
		insert into consumo values(4000123412340012::char(16), 0012::char(4), 7::int, 55000::decimal(7,2));
		insert into consumo values(4000123412340012::char(16), 0012::char(4), 8::int, 55000::decimal(7,2));
		--Entonces la tarjeta anterior deberia estas suspendida
		insert into consumo values(4000123412340012::char(16), 0012::char(4), 5::int, 5000::decimal(7,2));
		
		--Dos compras en comercios con mismo codigo postal con diferencia de menos de 1 minuto
		insert into consumo values(4000123412340005::char(16), 0005::char(4), 13::int, 1200::decimal(7,2));
		insert into consumo values(4000123412340005::char(16), 0005::char(4), 14::int, 1500::decimal(7,2));
		
		--Dos compras en comercios con distinto codigo postal con diferencia de menos de 5 minutos
		insert into consumo values(4000123412340008::char(16), 0008::char(4), 10::int, 3000::decimal(7,2));
		insert into consumo values(4000123412340008::char(16), 0008::char(4), 11::int, 2000::decimal(7,2));`)
    	if err != nil {
    	    log.Fatal(err)
    	}
	}else{
		fmt.Println("Los consumos de testeo ya están guardados en la base de datos 'informaciontarjeta'")
	}

}

func createAutorizarCompra(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Creo la funcion autorizarCompra en 'informaciontarjeta'
	_, err = db.Exec(`CREATE OR REPLACE FUNCTION autorizarCompra(numerotarjeta char(16), codigoseguridad char(4), nrocomercio int, monto decimal(7,2)) RETURNS BOOLEAN as $$
    DECLARE
		comprasTarjetaElegida compra%rowtype;
		tarjetaAux tarjeta%rowtype;
        cantcompras INT;
		cantrechazos INT;
		deudaTarjeta int;
		limiteTarjeta int;
	BEGIN
		deudaTarjeta = 0;
		limiteTarjeta = 0;
		cantrechazos = COUNT(*) from rechazo;
		cantrechazos = cantrechazos +1;
		
		--Confirmar que la tarjeta existe
        SELECT * INTO tarjetaAux from tarjeta where tarjeta.nrotarjeta = numerotarjeta;
        IF NOT FOUND THEN
            INSERT INTO rechazo VALUES(cantrechazos, numerotarjeta, nrocomercio, CURRENT_TIMESTAMP, monto, '?tarjeta no válida o no vigente');
            return FALSE;
		END IF;		
		
		--Confirmar que el código de seguridad de la tarjeta es correcto
        SELECT * into tarjetaAux from tarjeta where tarjeta.nrotarjeta = numerotarjeta and lpad(tarjeta.codseguridad,4,'0') = lpad(codigoseguridad,4,'0');
        IF NOT FOUND THEN
            INSERT INTO rechazo VALUES(cantrechazos, numerotarjeta, nrocomercio, CURRENT_TIMESTAMP, monto, '?código de seguridad inválido');
            return FALSE;
		END IF;
		
		--Confirmar que la compra no excede el límite de compra de la tarjeta 
		--Primero reviso las compras que se realizaron con la tarjeta y que estan sin pagar
		FOR comprasTarjetaElegida IN SELECT * from compra where compra.nrotarjeta = numerotarjeta and compra.pagado=FALSE LOOP
			deudaTarjeta = deudaTarjeta + comprasTarjetaElegida.monto;
		END LOOP;
		
		--Busco cual es el limite de la tarjeta
		FOR tarjetaAux IN SELECT * from tarjeta where tarjeta.nrotarjeta = numerotarjeta LOOP
			limiteTarjeta = limiteTarjeta + tarjetaAux.limitecompra;
		END LOOP;

		--Si la suma de lo que debe pagar la tarjeta mas el monto de la compra que se quiere
		--autorizar son mayores que el limite, entonces la compra es rechazada
		IF (limiteTarjeta-deudaTarjeta-monto)<0 THEN
			INSERT INTO rechazo VALUES(cantrechazos, numerotarjeta, nrocomercio, CURRENT_TIMESTAMP, monto, '?supera límite de tarjeta');
			return FALSE;
		END IF;
		
		--Confirmar que la tarjeta no está vencida
        FOR tarjetaAux IN SELECT * from tarjeta where tarjeta.nrotarjeta = numerotarjeta LOOP
			IF TO_DATE(tarjetaAux.validahasta,'YYYYMM') < CURRENT_DATE THEN
            	INSERT INTO rechazo VALUES(cantrechazos, numerotarjeta, nrocomercio, CURRENT_TIMESTAMP, monto, '?plazo de vigencia expirado');
				return FALSE;
            END IF;            
		END LOOP;
		
		--Confirmar que la tarjeta no se encuentra suspendida
        FOR tarjetaAux IN SELECT * from tarjeta where tarjeta.nrotarjeta = numerotarjeta LOOP
            IF tarjetaAux.estado = 'suspendida' THEN
                INSERT INTO rechazo VALUES(cantrechazos, numerotarjeta, nrocomercio, CURRENT_TIMESTAMP, monto, '?la tarjeta se encuentra suspendida');
                return FALSE;
            END IF;
        END LOOP;
		
		--Si no salta ningún problema entonces se realiza la compra y retorna true
		cantcompras = COUNT(*) from compra;
		cantcompras = cantcompras +1;
		INSERT INTO compra VALUES(cantcompras, numerotarjeta, nrocomercio, CURRENT_TIMESTAMP, monto, FALSE);
        RETURN TRUE;
    END;
	$$ LANGUAGE plpgsql`)
	if err != nil {
		log.Fatal(err)
	}
}

func createGenerarResumen(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Creo la funcion generarResumen en 'informaciontarjeta'
	_, err = db.Exec(`CREATE OR REPLACE FUNCTION generarResumen(numerocliente INT, periodo INT) RETURNS void as $$
	DECLARE
		clienteElegido cliente%rowtype;
        cierreElegido cierre%rowtype;
        tarjetasDelClienteElegido tarjeta%rowtype;
		comprasElegidas compra%rowtype;
		comerciosElegidos comercio%rowtype;
		cantcabeceras INT;
		nrolinea INT;
		total decimal(8,2);
	BEGIN
		total = 0;
		nrolinea = 1;
		cantcabeceras := COUNT(*) FROM cabecera;
		cantcabeceras = cantcabeceras + 1; 

		--Primero busco al cliente pedido para obtener sus datos para pasarlos cabecera y
		--todas sus tarjetas para poder obtener sus compras
		FOR clienteElegido IN SELECT * FROM cliente WHERE cliente.nrocliente = numerocliente LOOP
			FOR tarjetasDelClienteElegido IN SELECT * FROM tarjeta WHERE tarjeta.nrocliente = numerocliente LOOP
							
				--Busco el cierre del correspondiente mes del año actual para así poder obtener
				--las fechas de inicio y fin del cierre, lo cual permite obtener las compras
				--realizadas con las tarjeta entre dichas fechas y sumar el gasto total
				FOR cierreElegido IN SELECT * FROM cierre WHERE cierre.mes = periodo AND cierre.terminacion::text = RIGHT(tarjetasDelClienteElegido.nrotarjeta, 1) AND cierre.año::char(4) = to_char(Now(), 'YYYY') LOOP						  
					FOR comprasElegidas IN SELECT * FROM compra WHERE compra.nrotarjeta = tarjetasDelClienteElegido.nrotarjeta LOOP 
						total = total + comprasElegidas.monto;                   
					END LOOP;		
					
					--Ya puedo acceder a todos los datos necesario para insertar una fila en cabecera
					INSERT INTO cabecera VALUES(cantcabeceras, clienteElegido.nombre, clienteElegido.apellido, clienteElegido.domicilio, tarjetasDelClienteElegido.nrotarjeta, cierreElegido.fechainicio, cierreElegido.fechacierre, cierreElegido.fechavto, total);
							
					--Ahora para añadir las compras a detalle las vuelvo a recorrer y además,
					--busco el comercio donde se realizó cada compra porque detalle pide el nombre
					--de este.
					--Las filas añadidas en detalle tienen que tener el mismo nroresumen 
					--todas (cantcabeceras seria en este caso)
					--El nrolinea es uno distinto para cada fila añadida a detalle
					FOR comprasElegidas IN SELECT * FROM compra WHERE compra.nrotarjeta = tarjetasDelClienteElegido.nrotarjeta AND compra.fecha::date > cierreElegido.fechainicio AND compra.fecha::date < cierreElegido.fechacierre LOOP 
						For comerciosElegidos IN SELECT * FROM comercio WHERE comercio.nrocomercio = comprasElegidas.nrocomercio LOOP
							INSERT INTO detalle VALUES(cantcabeceras, nrolinea, comprasElegidas.fecha, comerciosElegidos.nombre, comprasElegidas.monto);
							nrolinea = nrolinea + 1;                        
						END LOOP;
					END LOOP;						
				END LOOP;
			END LOOP;    
		END LOOP; 
	END;
	$$ LANGUAGE plpgsql`)
	if err != nil {
		log.Fatal(err)
	}
}

func createAlertarClientesRechazo(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	_, err = db.Exec(`CREATE OR REPLACE FUNCTION alertarClientesRechazo() RETURNS trigger as $$
    DECLARE
		cantalertas INT;
		rechazoAux rechazo%rowtype;
	BEGIN
		cantalertas = COUNT(*) from alerta;
		cantalertas = cantalertas +1;

		--Al agregar un rechazo a la tabla rechazo, lo agrego a la tabla alerta
        INSERT INTO alerta VALUES(cantalertas, NEW.nrotarjeta, NEW.fecha, NEW.nrorechazo, 0, NEW.motivo);
		
		--Busco si entre los rechazos que ya estaban en la tabla rechazo hay otro con el
		--mismo nrotarjeta que sea por exceso de limite, de haberlo creo otra alerta y
		--suspendo la tarjeta correspondiente
		FOR rechazoAux IN SELECT * FROM rechazo WHERE rechazo.nrorechazo!=NEW.nrorechazo LOOP
            IF NEW.nrotarjeta = rechazoAux.nrotarjeta AND NEW.motivo = '?supera límite de tarjeta' AND rechazoAux.motivo = '?supera límite de tarjeta' AND DATE_PART('day', rechazoAux.fecha - NEW.fecha)<2 THEN
                INSERT INTO alerta VALUES(cantalertas+1, NEW.nrotarjeta, NEW.fecha, NEW.nrorechazo, 32, '?Doble rechazo por exceso de límite en un mismo día');
                UPDATE tarjeta SET estado = 'suspendida' WHERE tarjeta.nrotarjeta = NEW.nrotarjeta;
            END IF;
		END LOOP;
		RETURN NULL;	
	END;
    $$ LANGUAGE plpgsql;

CREATE TRIGGER alertarClientesRechazoTrigger
    AFTER INSERT ON rechazo
    FOR EACH ROW
    EXECUTE PROCEDURE alertarClientesRechazo();`)
	if err != nil {
		log.Fatal(err)
	}
}



func createAlertarClientesCompra(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()
	
	_, err = db.Exec(`CREATE OR REPLACE FUNCTION alertarClientesCompra() RETURNS trigger as $$
	DECLARE
        cantalertas INT;
		cantrechazos INT;
		compraAux compra%rowtype;
		comercioAux comercio%rowtype;
		comercioElegido comercio%rowtype;
	BEGIN
		cantalertas = COUNT(*) from alerta;
		cantalertas = cantalertas + 1;	

		--Tomo todas las compras que se realizaron con la tarjeta de la compra que activó el trigger
		FOR compraAux IN SELECT * FROM compra WHERE compra.nrotarjeta = NEW.nrotarjeta AND compra.nrocomercio != NEW.nrocomercio LOOP
			FOR comercioElegido IN SELECT * FROM comercio WHERE comercio.nrocomercio = NEW.nrocomercio LOOP
				FOR comercioAux IN SELECT * FROM comercio WHERE comercio.nrocomercio = compraAux.nrocomercio LOOP
					
					--Si una compra anterior con la misma tarjeta se realizó con una diferencia
					--menor a 1 minuto en otro comercio con el mismo codigo postal, 
					--creo una nueva alerta
					IF NEW.nrotarjeta = compraAux.nrotarjeta AND DATE_PART('minute', compraAux.fecha - NEW.fecha)<2 AND comercioAux.codigopostal = comercioElegido.codigopostal THEN
						INSERT INTO alerta VALUES(cantalertas, NEW.nrotarjeta, NEW.fecha, 0, 1, '?Se detectaron dos compras con menos de 1 minuto de diferencia en dos locales distintos con mismo código postal');
					
					--Si una compra anterior con la misma tarjeta se realizó con una diferencia
					--menor a 5 minutos en otro comercio con distinto codigo postal, 
					--creo una nueva alerta
					ELSIF NEW.nrotarjeta = compraAux.nrotarjeta AND DATE_PART('minute', compraAux.fecha - NEW.fecha)<5 AND comercioAux.codigopostal != comercioElegido.codigopostal THEN
						INSERT INTO alerta VALUES(cantalertas, NEW.nrotarjeta, NEW.fecha, 0, 2, '?Se detectaron dos compras con menos de 5 minutos de diferencia en dos locales con distinto código postal');
					END IF;
				END LOOP;
			END LOOP;
		END LOOP;
		RETURN NULL;            
    END;
    $$ LANGUAGE plpgsql;

CREATE TRIGGER alertarClientesCompraTrigger
    AFTER INSERT ON compra
    FOR EACH ROW
    EXECUTE PROCEDURE alertarClientesCompra();`)
	if err != nil {
		log.Fatal(err)
	}

}

func createGenerarConsumos(){
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()
	
	_, err = db.Exec(`CREATE OR REPLACE FUNCTION generarconsumosprueba() RETURNS void as $$
	DECLARE
		consumoAux consumo%rowtype;
	BEGIN
		
		FOR consumoAux IN SELECT * FROM consumo LOOP
			PERFORM "autorizarcompra"(consumoAux.nrotarjeta, consumoAux.codseguridad, consumoAux.nrocomercio, consumoAux.monto);
		END LOOP;      

    END;
	$$ LANGUAGE plpgsql;`)

	if err != nil {
		log.Fatal(err)
	}

}
	

func main() {

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=informaciontarjeta sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()

	//Utilizamos un Reader para que el usuario pueda manejar la base de datos y pueda utilizar las funciones autorizarCompra y generarResumen desde la CLI de Go.

	reader := bufio.NewReader(os.Stdin)
  	fmt.Println("Información tarjeta de crédito")
	fmt.Println("---------------------")
	fmt.Println("1-Crear Base de Datos 'informaciontarjeta'")
	fmt.Println("2-Borrar Base de Datos 'informaciontarjeta'")
	fmt.Println("3-Crear Tablas de 'informaciontarjeta'")	
	fmt.Println("4-Crear todas las funciones de 'informaciontarjeta'")
	fmt.Println("5-Crear clientes/tarjetas/comercions/cierres/consumos de prueba en 'informaciontarjeta'")
	fmt.Println("6-Crear todas las PK's de 'informaciontarjeta'")
	fmt.Println("7-Crear todas las FK's de 'informaciontarjeta'")	
	fmt.Println("8-Borrar todas las PK's de 'informaciontarjeta'")
	fmt.Println("9-Borrar todas las FK's de 'informaciontarjeta'")
	fmt.Println("10-Generar compras de prueba")
	fmt.Println("11-Realizar compra")
	fmt.Println("12-Generar resumen")
	
		
	fmt.Println("0-Salir")

  	for {
  		fmt.Print("-> ")
    	text, _ := reader.ReadString('\n')
    	// convert CRLF to LF
    	text = strings.Replace(text, "\n", "", -1)

    	if strings.Compare("0", text) == 0 {
			os.Exit(0)
    	}else if strings.Compare("1", text) == 0{
			createDatabase()
		}else if strings.Compare("2", text) == 0{
			deleteDatabase()
		}else if strings.Compare("3", text) == 0{
			createTables()
		}else if strings.Compare("4", text) == 0{
			createAutorizarCompra()
			createGenerarResumen()
			createAlertarClientesRechazo()
			createAlertarClientesCompra()
			createGenerarConsumos()
		}else if strings.Compare("5", text) == 0{
			createTestClientes()
			createTestComercios()
			createTestTarjetas()
			createTestCierres()
			createTestConsumos()
		}else if strings.Compare("6", text) == 0{

			_,err = db.Exec(`ALTER TABLE cliente ADD CONSTRAINT clientepk PRIMARY KEY (nrocliente);
			ALTER TABLE tarjeta ADD CONSTRAINT tarjetapk PRIMARY KEY (nrotarjeta);
			ALTER TABLE comercio ADD CONSTRAINT comerciopk PRIMARY KEY (nrocomercio);
			ALTER TABLE compra ADD CONSTRAINT comprapk PRIMARY KEY (nrooperacion);
			ALTER TABLE rechazo ADD CONSTRAINT rechazopk PRIMARY KEY (nrorechazo);
			ALTER TABLE alerta ADD CONSTRAINT alertapk PRIMARY KEY (nroalerta);
			ALTER TABLE cabecera ADD CONSTRAINT cabecerapk PRIMARY KEY (nroresumen);
			ALTER TABLE detalle ADD CONSTRAINT detallepk PRIMARY KEY (nroresumen,nrolinea);
			ALTER TABLE cierre ADD CONSTRAINT cierrepk PRIMARY KEY (año,mes,terminacion);`)
			if err != nil {
				log.Fatal(err)
			}
		}else if strings.Compare("7", text) == 0{

			_,err = db.Exec(`ALTER TABLE detalle ADD CONSTRAINT detallefk FOREIGN KEY (nroresumen) REFERENCES cabecera (nroresumen) ON DELETE CASCADE;`)
			if err != nil {
				log.Fatal(err)
			}
		}else if strings.Compare("8", text) == 0{

			_,err = db.Exec(`ALTER TABLE cliente DROP CONSTRAINT clientepk;
			ALTER TABLE tarjeta DROP CONSTRAINT tarjetapk;
			ALTER TABLE comercio DROP CONSTRAINT comerciopk;
			ALTER TABLE compra DROP CONSTRAINT comprapk;
			ALTER TABLE rechazo DROP CONSTRAINT rechazopk;
			ALTER TABLE alerta DROP CONSTRAINT alertapk;
			ALTER TABLE cabecera DROP CONSTRAINT cabecerapk;
			ALTER TABLE detalle DROP CONSTRAINT detallepk;
			ALTER TABLE cierre DROP CONSTRAINT cierrepk;`)
			if err != nil {
				log.Fatal(err)
			}
		}else if strings.Compare("9", text) == 0{

			_,err = db.Exec(`ALTER TABLE detalle DROP CONSTRAINT detallefk;`)
			if err != nil {
				log.Fatal(err)
			}
		}else if strings.Compare("10", text) == 0{
			db.Query(`SELECT "generarconsumosprueba"();`)
		}else if strings.Compare("11", text) == 0{
			fmt.Print("Ingrese el número de tarjeta -> ")
			nrotarjeta, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el código de seguridad de la tarjeta -> ")
			codseguridad, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el número del comercio -> ")
			nrocomercio, _ := reader.ReadString('\n')
			fmt.Print("Ingrese monto a pagar -> ")
			monto, _ := reader.ReadString('\n')
			stmt, err := db.Prepare(`SELECT "autorizarcompra"($1::char(16), $2::char(4), $3::int, $4::decimal(7,2));`)
			if err != nil {
		 	  log.Fatal(err)
			}
			stmt.Query(nrotarjeta,codseguridad,nrocomercio,monto)
		}else if strings.Compare("12", text) == 0{
			fmt.Print("Ingrese el número de cliente -> ")
			nrocliente, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el mes del periodo -> ")
			mes, _ := reader.ReadString('\n')
			stmt, err := db.Prepare(`SELECT "generarresumen"($1::int, $2::int);`)
			if err != nil {
		 	  log.Fatal(err)
			}
			stmt.Query(nrocliente,mes)
		}
  	}	  
	
}
