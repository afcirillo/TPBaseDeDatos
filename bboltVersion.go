package main

import (
    "encoding/json"
    "fmt"
    bolt "github.com/boltdb/bolt"
    "log"
	"strconv"
	"bufio"
	"os"
	"strings"
)

type Cliente struct{
    Nrocliente   int `json:"nrocliente"`
    Nombre   string `json:"nombre"`
	Apellido string `json:"apellido"`
	Domicilio   string `json:"domicilio"`
    Telefono string `json:"telefono"`
}

type Tarjeta struct {
    Nrotarjeta   int `json:"nrotarjeta"`
    Nrocliente   string `json:"nrocliente"`
	Validadesde string `json:"validadesde"`
	Validahasta string `json:"validahasta"`
	Codseguridad string `json:"codseguridad"`
	Limitecompra string `json:"limitecompra"`
	Estado string `json:"estado"`
}

type Comercio struct {
    Nrocomercio   int `json:"nrocomercio"`
    Nombre   string `json:"nombre"`
	Domicilio string `json:"domicilio"`
	Codigopostal   string `json:"codigopostal"`
    Telefono string `json:"telefono"`
}

type Compra struct {
    Nrooperacion   int `json:"nrooperacion"`
    Nrotarjeta   int `json:"nrotarjeta"`
	Nrocomercio int `json:"nrocomercio"`
	Fecha  string `json:"fecha"`
	Monto string `json:"monto"`
	Pagado string `json:"pagado"`
}

type Rechazo struct {
    Nrorechazo   int `json:"nrorechazo"`
    Nrotarjeta   rune `json:"nrotarjeta"`
	Nrocomercio int `json:"nrocomercio"`
	Fecha   string `json:"fecha"`
	Monto rune `json:"monto"`
	Motivo string `json:"motivo"`
}

type Cierre struct {
    Año   int `json:"año"`
    Mes   int `json:"mes"`
	Terminacion int `json:"terminacion"`
	Fechainicio   string `json:"fechainicio"`
	Fechacierre string `json:"fechacierre"`
	Fechavto   string `json:"fechavto"`
}

type Cabecera struct {
    Nroresumen   int `json:"nroresumen"`
    Nombre   string `json:"nombre"`
	Apellido string `json:"apellido"`
	Domicilio   string `json:"domicilio"`
	Nrotarjeta rune `json:"nrotarjeta"`
	Desde   string `json:"desde"`
	Hasta string `json:"hasta"`
	Vence   string `json:"vence"`
    Total rune `json:"total"`
}

type Detalle struct {
    Nroresumen   int `json:"nroresumen"`
    Nrolinea   int `json:"nrolinea"`
	Fecha string `json:"fecha"`
	Nombrecomercio   string `json:"nombrecomercio"`
    Monto rune `json:"monto"`
}

type Alerta struct {
    Nroalerta   int `json:"nroalerta"`
    Nrotarjeta   rune `json:"nrotarjeta"`
	Fecha string `json:"fecha"`
	Nrorechazo   int `json:"nrorechazo"`
	Codalerta int `json:"codalerta"`
	Descripcion string `json:"descripcion"`
}

type Consumo struct {
    Nrotarjeta   rune `json:"nrocliente"`
    Codseguridad   rune `json:"nombre"`
	Nrocomercio int `json:"apellido"`
    Monto rune `json:"telefono"`
}

func añadirFila(db *bolt.DB, bucketName string, key []byte, val []byte) error {
    // abre transacción de escritura
    tx, err := db.Begin(true)
    if err != nil {
        return err
    }
    defer tx.Rollback()

    b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

    err = b.Put(key, val)
    if err != nil {
        return err
    }

    // cierra transacción
    if err := tx.Commit(); err != nil {
        return err
    }

    return nil
}

func agregarClientes(db *bolt.DB, numero int, nombre string, apellido string, domicilio string, telefono string){
	
	nuevoCliente := Cliente{numero, nombre, apellido, domicilio, telefono}
	data, err := json.Marshal(nuevoCliente)
    if err != nil {
        log.Fatal(err)
	}
	

	añadirFila(db, "Cliente", []byte(strconv.Itoa(nuevoCliente.Nrocliente)), data)

}

func agregarTarjetas(db *bolt.DB, numero int, numerocliente string, validadesde string, validahasta string, codseg string, limite string, estado string){
	
	nuevaTarjeta := Tarjeta{numero, numerocliente, validadesde, validahasta, codseg, limite, estado}
	data, err := json.Marshal(nuevaTarjeta)
    if err != nil {
        log.Fatal(err)
    }

	añadirFila(db, "tarjeta", []byte(strconv.Itoa(nuevaTarjeta.Nrotarjeta)), data)

}

func agregarComercios(db *bolt.DB, numero int, nombre string, domicilio string, cp string, telefono string){
	
	nuevoComercio := Comercio{numero, nombre, domicilio, cp, telefono}
	data, err := json.Marshal(nuevoComercio)
    if err != nil {
        log.Fatal(err)
    }

	añadirFila(db, "comercio", []byte(strconv.Itoa(nuevoComercio.Nrocomercio)), data)

}

func agregarCompra(db *bolt.DB, numero int, numtarjeta int, numcomercio int, fecha string, monto string, estado string){
	
	nuevaCompra := Compra{numero, numtarjeta, numcomercio, fecha, monto,estado}
	data, err := json.Marshal(nuevaCompra)
    if err != nil {
        log.Fatal(err)
    }

	añadirFila(db, "compra", []byte(strconv.Itoa(nuevaCompra.Nrooperacion)), data)

}



func main() {
    db, err := bolt.Open("informaciontarjeta.db", 0600, nil)
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()
	
	reader := bufio.NewReader(os.Stdin)
  	fmt.Println("Información tarjeta de crédito")
	fmt.Println("---------------------")
	fmt.Println("1-Agregar cliente en informaciontarjeta")
	fmt.Println("2-Agregar tarjeta en informaciontarjeta")
	fmt.Println("3-Agregar comercio en informaciontarjeta")
	fmt.Println("4-Agregar compra en informaciontarjeta")
	fmt.Println("0-Salir")

  	for {
  		fmt.Print("-> ")
    	text, _ := reader.ReadString('\n')
    	// convert CRLF to LF
    	text = strings.Replace(text, "\n", "", -1)

    	if strings.Compare("0", text) == 0 {
			os.Exit(0)
    	}else if strings.Compare("1", text) == 0{
			fmt.Print("Ingrese el número de cliente -> ")
			numero, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el nombre del cliente -> ")
			nombre, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el apellido del cliente -> ")
			apellido, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el domicilio del cliente -> ")
			domicilio, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el teléfono del cliente -> ")
			telefono, _ := reader.ReadString('\n')
			id, err := strconv.Atoi(numero)
			if err == nil {
				log.Fatal(err)
			}
			agregarClientes(db,id,nombre,apellido,domicilio,telefono)
		}else if strings.Compare("2", text) == 0{
			fmt.Print("Ingrese el número de tarjeta -> ")
			numero, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el numero del cliente de la tarjeta-> ")
			numerocliente, _ := reader.ReadString('\n')
			fmt.Print("Ingrese la fecha de inicio -> ")
			validadesde, _ := reader.ReadString('\n')
			fmt.Print("Ingrese la fecha de expiración -> ")
			validahasta, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el código de seguridad -> ")
			codigo, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el limite -> ")
			limite, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el estado -> ")
			estado, _ := reader.ReadString('\n')
			id, err := strconv.Atoi(numero)
			if err == nil {
				log.Fatal(err)
			}
			agregarTarjetas(db,id,numerocliente,validadesde,validahasta,codigo,limite,estado)
		}else if strings.Compare("3", text) == 0{
			fmt.Print("Ingrese el número de comercio -> ")
			numero, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el nombre del comercio -> ")
			nombre, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el domicilio del comercio -> ")
			domicilio, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el  codigo postal del comercio -> ")
			cp, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el teléfono del comercio -> ")
			telefono, _ := reader.ReadString('\n')
			id, err := strconv.Atoi(numero)
			if err == nil {
				log.Fatal(err)
			}
			agregarComercios(db,id,nombre,domicilio,cp,telefono)
		}else if strings.Compare("4", text) == 0{			
			fmt.Print("Ingrese el número de comercio -> ")
			numero, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el nombre del comercio -> ")
			numtarjeta, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el domicilio del comercio -> ")
			numcomercio, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el  codigo postal del comercio -> ")
			fecha, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el  codigo postal del comercio -> ")
			monto, _ := reader.ReadString('\n')
			fmt.Print("Ingrese el teléfono del comercio -> ")
			pagado, _ := reader.ReadString('\n')
			id, err := strconv.Atoi(numero)
			if err == nil {
				log.Fatal(err)
			}
			id2, err := strconv.Atoi(numtarjeta)
			if err == nil {
				log.Fatal(err)
			}
			id3, err := strconv.Atoi(numcomercio)
			if err == nil {
				log.Fatal(err)
			}
			agregarCompra(db,id,id2,id3,fecha,monto,pagado)
		}
	}

}

